kontrast (24.12.0-1) unstable; urgency=medium

  [ Debian Qt/KDE Maintainers ]
  * New upstream release (24.08.1).

  [ Scarlett Moore ]
  * Add myself to uploaders.
  * Update dependencies according to cmake.
  * Refresh copyright.
  * Bump standards version 4.7.0; no chsnges required.

  [ Aurélien COUDERC ]
  * New upstream release (24.12.0).
  * Update build-deps and deps with the info from cmake.

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 24 Dec 2024 00:03:54 +0100

kontrast (22.12.3-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (22.12.3).
  * Update build-deps and deps with the info from cmake.

 -- Aurélien COUDERC <coucouf@debian.org>  Wed, 01 Mar 2023 11:57:56 +0100

kontrast (22.12.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Re-export upstream signing key without extra signatures.
  * Modernize building:
    - add the dh-sequence-kf5 build dependency to use the kf5 addon
      automatically, removing pkg-kde-tools
    - drop all the manually specified addons and buildsystem for dh

 -- Pino Toscano <pino@debian.org>  Sun, 05 Feb 2023 00:39:22 +0100

kontrast (22.12.1-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (22.12.1).
  * Bump Standards-Version to 4.6.2, no change required.
  * Add Albert Astals Cid’s master key to upstream signing keys.

 -- Aurélien COUDERC <coucouf@debian.org>  Fri, 06 Jan 2023 23:38:27 +0100

kontrast (22.12.0-2) unstable; urgency=medium

  * Upload with a correctly released changelog.

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 10 Dec 2022 16:04:49 +0100

kontrast (22.12.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (22.08.3).
  * Update build-deps and deps with the info from cmake.
  * Bump Standards-Version to 4.6.1, no change required.
  * New upstream release (22.11.90).
  * Update build-deps and deps with the info from cmake.
  * New upstream release (22.12.0).
  * Refresh lintian overrides.

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 10 Dec 2022 00:13:01 +0100

kontrast (21.12.3-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (21.12.3).
  * Reenable blhc in Salsa CI, as it should now work.
  * Refresh copyright information.

 -- Aurélien COUDERC <coucouf@debian.org>  Mon, 07 Mar 2022 08:36:09 +0100

kontrast (21.08.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update the upstream GPG signing key.
  * Bump Standards-Version to 4.6.0, no changes required.

 -- Pino Toscano <pino@debian.org>  Sat, 28 Aug 2021 12:36:24 +0200

kontrast (20.12.1-1) unstable; urgency=medium

  * Initial release (Closes: #979568).

 -- Aurélien COUDERC <coucouf@debian.org>  Fri, 08 Jan 2021 13:48:20 +0100
